#pragma once
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <unistd.h>
static size_t pages_count(size_t mem);
static size_t round_pages(size_t mem);
static size_t region_actual_size(size_t query);
enum test_settings
{
	NORMAL_MEMORY_ALLOCATION=0,
	FREE_ONE_BLOCK,
	FREE_TWO_BLOCKS,
	ALLOCATION_OF_A_NEW_REGION,
	ALLOCATION_OF_A_NEW_REGION_IN_A_NEW_LOCATION
};
void run_test_to_check_NORMAL_MEMORY_ALLOCATION();
void run_test_to_check_FREE_ONE_BLOCK();
void run_test_to_check_FREE_TWO_BLOCKS();
void run_test_to_check_ALLOCATION_OF_A_NEW_REGION();
void run_test_to_check_ALLOCATION_OF_A_NEW_REGION_IN_A_NEW_LOCATION();
void print_test_status(char* const message);
