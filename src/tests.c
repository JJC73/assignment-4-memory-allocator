#include "tests.h"
#include <stdio.h>
#include <stdlib.h>

#define MEMORY_CAPACITY 8192
void free_heap(void* heap_address, size_t const size) {
	munmap(heap_address, size_from_capacity((block_capacity) { size }).bytes);
}
void print_test_status(char* const message) {
	printf("%s" "%s", message, "\n");
	printf("%s", "-------------------------------------------");
}

void run_test_to_check_NORMAL_MEMORY_ALLOCATION() {
	print_test_status("Test for checking memory allocation");
	void* init_heap = heap_init(MEMORY_CAPACITY);
	if (!init_heap) {
		print_test_status("Error: the heap was not initialized");
		return;
	}
	print_test_status("The heap was allocated");
	debug_heap(stdout, init_heap);
	void* check_malloc = _malloc(20);
	if (!check_malloc) {
		print_test_status("Error: the memory was not allocated");
		return;
	}
	print_test_status("The memory was allocated");
	debug_heap(stdout, init_heap);
	_free(check_malloc);
	print_test_status("The memory was been cleared");
	debug_heap(stdout, init_heap);
	free_heap(init_heap, MEMORY_CAPACITY);
	print_test_status("The heap was been cleared");
	print_test_status("Test was successful");
}

void run_test_to_check_FREE_ONE_BLOCK() {
	print_test_status("Test for freeing up memory in one block");
	void* init_heap = heap_init(MEMORY_CAPACITY);
	if (!init_heap) {
		print_test_status("Error: the heap was not initialized");
		return;
	}
	print_test_status("The heap was allocated");
	debug_heap(stdout, init_heap);
	void* check_malloc = _malloc(20);
	void* check_malloc1 = _malloc(54);
	if (!check_malloc || !check_malloc1) {
		print_test_status("Error: the memory was not allocated");
		return;
	}
	print_test_status("The memory was allocated");
	debug_heap(stdout, init_heap);
	_free(check_malloc);
	print_test_status("The first block was released");
	debug_heap(stdout, init_heap);
	_free(check_malloc1);
	print_test_status("The second block was released");
	debug_heap(stdout, init_heap);
	free_heap(init_heap, MEMORY_CAPACITY);
	print_test_status("The heap was been cleared");
	print_test_status("Test was successful");

}
void run_test_to_check_FREE_TWO_BLOCKS() {
	print_test_status("test for freeing up memory in several blocks");
	void* init_heap = heap_init(MEMORY_CAPACITY);
	if (!init_heap) {
		print_test_status("Error: the heap was not initialized");
		return;
	}
	print_test_status("The heap was allocated");
	debug_heap(stdout, init_heap);
	void* check_malloc = _malloc(20);
	void* check_malloc1 = _malloc(54);
	void* check_malloc2 = _malloc(540);
	if (!check_malloc || !check_malloc1 || !check_malloc2) {
		print_test_status("Error: the memory was not allocated");
		return;
	}
	print_test_status("The memory was allocated");
	debug_heap(stdout, init_heap);
	_free(check_malloc1);
	print_test_status("The second block was released");
	debug_heap(stdout, init_heap);
	_free(check_malloc);
	print_test_status("The first block was released");
	debug_heap(stdout, init_heap);
	_free(check_malloc2);
	print_test_status("The memory was been cleared");
	free_heap(init_heap, MEMORY_CAPACITY);
	print_test_status("The heap was been cleared");
	print_test_status("Test was successful");

}
void run_test_to_check_ALLOCATION_OF_A_NEW_REGION() {
	void* init_heap = heap_init(MEMORY_CAPACITY);
	if (!init_heap) {
		print_test_status("Error: the heap was not initialized");
		return;
	}
	print_test_status("The heap was allocated");
	debug_heap(stdout, init_heap);
	void* check_malloc = _malloc(MEMORY_CAPACITY);
	void* check_malloc1 = _malloc(MEMORY_CAPACITY);
	if (!check_malloc || !check_malloc1) {
		print_test_status("Error: the memory was not allocated");
		return;
	}
	struct block_header* header = (struct block_header*)(check_malloc - offsetof(struct block_header, contents));
	struct block_header* header1 = (struct block_header*)(check_malloc1 - offsetof(struct block_header, contents));

	if (header->next != header1) {
		print_test_status("Error: blocks do not follow each other");
		return;
	}
	print_test_status("blocks do follow each other");
	debug_heap(stdout, init_heap);
	_free(check_malloc);
	_free(check_malloc1);
	print_test_status("The memory was been cleared");
	free_heap(init_heap, MEMORY_CAPACITY);
	print_test_status("The heap was been cleared");
	print_test_status("Test was successful");


}

void run_test_to_check_ALLOCATION_OF_A_NEW_REGION_IN_A_NEW_LOCATION() {
	void* init_heap = heap_init(MEMORY_CAPACITY);
	if (!init_heap) {
		print_test_status("Error: the heap was not initialized");
		return;
	}
	print_test_status("The heap was allocated");
	void* check_malloc = _malloc(MEMORY_CAPACITY);
	struct block_header* header = (struct block_header*)(check_malloc - offsetof(struct block_header, contents));
	void* buffer_heap = mmap((void*)(header->contents + header->capacity.bytes), MEMORY_CAPACITY, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
	if (!buffer_heap)
	{
		print_test_status("Error: the buffer head was not initialized");
		return;
	}
	void* check_malloc1 = _malloc(MEMORY_CAPACITY);
	if (!check_malloc) {
		print_test_status("Error: the memory wasn't allocation");
		return;
	}
	print_test_status("Successful");
	_free(check_malloc);
	_free(check_malloc1);
	free_heap(init_heap, MEMORY_CAPACITY);
	free_heap(buffer_heap, MEMORY_CAPACITY);
	print_test_status("The heap was been cleared");
	print_test_status("Test was successful");



}
