#include "tests.h"


#define run_test(settings) run_test_to_check_##settings()


int main() {
	run_test(NORMAL_MEMORY_ALLOCATION);
	run_test(FREE_ONE_BLOCK);
	run_test(FREE_TWO_BLOCKS);
	run_test(ALLOCATION_OF_A_NEW_REGION);
	run_test(ALLOCATION_OF_A_NEW_REGION_IN_A_NEW_LOCATION);
}
